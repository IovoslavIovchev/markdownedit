function getRenderedContent() {
  const content = document.getElementById("editor").value;
  return marked(content);
}

function render() {
  const rendered = document.getElementById("rendered").innerHTML = getRenderedContent();
}

function toggleRenderedMode() {
  const editor = document.getElementById("editor");
  const rendered = document.getElementById("rendered");

  rendered.innerHTML = "<p>Rendering..</p>";

  editor.classList.toggle("hidden");
  rendered.classList.toggle("hidden");

  const renderBtn = document.getElementById("rendered-mode");

  if (!rendered.classList.contains("hidden")) {
    render();
    renderBtn.textContent = "Editor";
  } else {
    renderBtn.textContent = "Markdown";
  }
}

function toggleMinimalisticMode() {
  const header = document.getElementById("header");
  const minModeBtn = document.getElementById("min-mode");

  header.classList.toggle("hidden");
  minModeBtn.textContent = header.classList.contains("hidden")
    ? "Full"
    : "Minimalistic";
}

function persistContent() {
  const content = document.getElementById("editor").value;

  const encoded = btoa(content);

  localStorage.setItem('content', encoded);
}

function getPersistedContent() {
  const encoded = localStorage.getItem('content');

  if (!encoded) {
    return;
  }

  const decoded = atob(encoded);

  document.getElementById("editor").value = decoded;

  localStorage.removeItem('content');
}

const buttonHandlers = {
  "min-mode": toggleMinimalisticMode,
  "rendered-mode": toggleRenderedMode
};

function init() {
  getPersistedContent();

  Object.keys(buttonHandlers).forEach(btnId =>
    document
      .getElementById(btnId)
      .addEventListener("click", buttonHandlers[btnId])
  );

  // render the markdown before printing
  window.addEventListener('beforeprint', render);

  window.addEventListener('beforeunload', persistContent)
}

init();
